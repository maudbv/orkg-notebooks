# Notebooks for Federated GraphQL

The queries given in the notebooks are excutable on federated GraphQL endpoint: https://www.orkg.org/orkg/graphql-federated

The current endpoint federates only ORKG, DataCite and GeoNames.

Notebooks:

COVID-19 R0 estimate [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/TIBHannover%2Forkg%2Forkg-notebooks/master?filepath=graphql%2FCOVID-19%20R0%20estimate%2FCOVID-19_R0_meta-data_analysis.ipynb)

Co-author network analysis [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/TIBHannover%2Forkg%2Forkg-notebooks/master?filepath=graphql%2FCOVID-19%20co-authors%20network%2FConditional_co-authors_network_analysis.ipynb)
